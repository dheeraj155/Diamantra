import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddarchitectComponent } from './addarchitect.component';

describe('AddarchitectComponent', () => {
  let component: AddarchitectComponent;
  let fixture: ComponentFixture<AddarchitectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddarchitectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddarchitectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
