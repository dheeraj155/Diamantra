import { Component, OnInit,NgModule,ElementRef} from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { datacalls } from '../datacalls.service';

@Component({
  selector: 'app-addarchitect',
  templateUrl: './addarchitect.component.html',
  styleUrls: ['./addarchitect.component.css'],
  providers:[datacalls]
})
export class AddarchitectComponent implements OnInit {
  architectForm;
  alertClass;
  errorMsg;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _datacalls: datacalls,private Router:Router) { }

  ngOnInit() {

    this.architectForm = this._fb.group({
      id:[0],
      name:[''],
      app_id:[16],
      email:[''],
      mobile_no:[''],
      password:[''],
    });
  }


  onSubmit(){
debugger
    var formData = new FormData();
    
     formData.append("id", this.architectForm.value.id);
     formData.append("app_id", this.architectForm.value.app_id);
     formData.append("name", this.architectForm.value.name);
     formData.append("email",this.architectForm.value.email);
     formData.append("mobile_no", this.architectForm.value.mobile_no);
     formData.append("password", this.architectForm.value.password);
     formData.append("usertype","architect" );


     console.log('form',formData)
     
      this._datacalls.addUser(formData).subscribe(posts => {
           
          if(posts['Success']==true){
                  this.alertClass = "alert alert-success text-center alert-dismissible";
                 this.errorMsg = 'Architect Added SuccessFully';
                   window.setTimeout(() => {
                 this.Router.navigate(['architect']);
               }, 2000);
               
               } 
               else{
                 this.alertClass = "alert alert-danger text-center alert-dismissible";
                 this.errorMsg = 'Form not Filled properly';
     
               }
         
           console.log('Run succesfull');
             
       });
     
     
  
  }
}
