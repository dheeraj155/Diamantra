import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbrochuresComponent } from './addbrochures.component';

describe('AddbrochuresComponent', () => {
  let component: AddbrochuresComponent;
  let fixture: ComponentFixture<AddbrochuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbrochuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbrochuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
