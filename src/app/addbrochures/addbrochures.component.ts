import { Component, OnInit,NgModule,ElementRef} from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute,Router} from '@angular/router';
import { datacalls } from '../datacalls.service';

@Component({
  selector: 'app-addbrochures',
  templateUrl: './addbrochures.component.html',
  styleUrls: ['./addbrochures.component.css'],
  providers:[datacalls]
})
export class AddbrochuresComponent implements OnInit {

  brochuresForm;
  alertClass;
  errorMsg;
  filesToUpload;
  dickson_brochures_id;
  imagepath1;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _datacalls: datacalls,private Router:Router) {

    this.brochuresForm = this._fb.group({
      dickson_brochures_id:[0],
      title:[''],
      app_id:[16],
      image1:[''],
      description:['']
    });
   }

  ngOnInit() {

    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.dickson_brochures_id = param['dickson_brochures_id'];
        console.log(this)
        if (this.dickson_brochures_id != undefined) {

          this._datacalls.getBrochures(this.dickson_brochures_id).subscribe(
            
            data => {
                            
                             console.log( 'data',data.Data[0].image);

                              
                 
                this.brochuresForm = this._fb.group({
                  'dickson_brochures_id': data.Data[0].dickson_brochures_id,
                  'app_id': data.Data[0].app_id,
                  'title': data.Data[0].title,
                  'image1': data.Data[0].image,
                  'description':data.Data[0].description
                  });
                this.filesToUpload=new Array<File>();
                this.imagepath1= this.brochuresForm.value.image1;
                console.log('path',this.imagepath1); 
                
                              }
              
            
          );
        }
      });
        

  }

  onChange(fileInput: any) {
    this.filesToUpload = fileInput.target.files['0'];
  //  this.isFileLoaded = true;
 
 }
 
  onSubmit(){

    var form = new FormData();
    form.append("dickson_brochures_id",this.brochuresForm.value.dickson_brochures_id);
    form.append("app_id", "16");
    form.append("title", this.brochuresForm.value.title);
    form.append("file1",this.filesToUpload );
    form.append("description", this.brochuresForm.value.description);
    // form.append("file2", this.selectimageToUpload);
    // form.append("table_type", 'training');
    
 
 console.log('form',form)
 
  this._datacalls.addbrochures(form).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
             this.errorMsg = 'Brochures Added SuccessFully';
               window.setTimeout(() => {
             this.Router.navigate(['brochures']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
 
  }
}
