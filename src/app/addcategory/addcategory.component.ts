import { Component, OnInit, ChangeDetectorRef   } from '@angular/core';
import {FormGroup,FormBuilder,FormControl,Validators} from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({

  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.css'],
  providers:[datacalls]
})
export class AddcategoryComponent implements OnInit {
 public categoryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    arya_category_id;
    filesToUpload;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    isSubcat:boolean;
    editSubcat:boolean;
    editCat:boolean;    
    parent_id;
    cat_id;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.categoryForm = this.fb.group({
            arya_category_id:[0],
            //app_id:[23],
            category_name: ['', [<any>Validators.required, <any>Validators.minLength(3)]],
            file:[''],
            image_path:['']

        });
     }


    ngOnInit() {

  console.log('hello')
  debugger
      // this.activatedRoute.params.subscribe(
      //   (param: any) => {
  
      //     this.image_slider_id = param['image_slider_id'];

      this.arya_category_id=this.activatedRoute.snapshot.params['id']
          console.log(this)
          if (this.arya_category_id != undefined) {
  
            this._datacalls.getCategory().subscribe(
              
              data => {
                              
                               console.log( 'data',data.Data[0].image);
  
                                
                   
                  this.categoryForm = this._fb.group({
                    'arya_category_id': data.Data[0].arya_category_id,
                    //'app_id': 23,
                    'category_name': data.Data[0].category_name,
                    'image1': data.Data[0].image_path
                    });
                  this.filesToUpload=new Array<File>();
                  this.imagepath1= this.categoryForm.value.image1;
                  console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
      if (this.activatedRoute.routeConfig.path == 'addsubcategory/:parent_id') {
        this.isSubcat = true;
    } else {
        this.isSubcat = false;
    }
    if (this.activatedRoute.routeConfig.path == 'category/addsubcategory/:parent_id/:id') {

        this.editSubcat = true;
    }
    if (this.activatedRoute.routeConfig.path == 'category/addcategory/:id') {

        this.editCat = true;
    } 
  
    }

 

  

    

    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }



  onSubmit() {
     
    var formData = new FormData();


    if (this.activatedRoute.routeConfig.path == 'addsubcategory/:parent_id') {
      this.isSubcat = true;
      this.categoryForm.value.parent_id = this.parent_id;
  } 
    
  else if(this.activatedRoute.routeConfig.path == 'editsubcategory/:id'){
this.categoryForm.value.category_id = this.id;
  }
  else {
      this.isSubcat = false;
      this.categoryForm.value.parent_id = '0'
  }

  if (this.activatedRoute.routeConfig.path == 'category/addsubcategory/:parent_id/:id') {

      console.log('category_id: '+ this.cat_id);
      this.categoryForm.value.category_id = this.cat_id;
      this.categoryForm.value.parent_id = this.parent_id;
  }


  if (this.categoryForm.value.category_id==""){
    this.categoryForm.value.category_id=0;
  }
   
    formData.append("arya_category_id", this.categoryForm.value.arya_category_id);
    formData.append("arya_app_id", '23');
    formData.append("category_name", this.categoryForm.value.category_name);
    formData.append("file1",this.filesToUpload);
    formData.append("image1",this.imagepath1);
    // formData.append("file2",this.filesToUpload2);
    formData.append("parent_id", '0');
 
 console.log('form',formData)
 
  this._datacalls.addCategory(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
             this.errorMsg = 'Category Added SuccessFully';
               window.setTimeout(() => {
             this.Router.navigate(['category']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }

