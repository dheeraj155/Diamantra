import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcolorimagesComponent } from './addcolorimages.component';

describe('AddcolorimagesComponent', () => {
  let component: AddcolorimagesComponent;
  let fixture: ComponentFixture<AddcolorimagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcolorimagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcolorimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
