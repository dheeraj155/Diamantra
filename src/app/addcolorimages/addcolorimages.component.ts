import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router'

@Component({
  selector: 'app-addcolorimages',
  templateUrl: './addcolorimages.component.html',
  styleUrls: ['./addcolorimages.component.css'],
  providers:[datacalls]
})
export class AddcolorimagesComponent implements OnInit {
  public imageForm: FormGroup;
  product_id;
  colorimages_id;
  errorMsg;
  alertClass;
  filesToUpload;
  imagepath8;
  jsonPara = {"app_id":16,"cat_id":null,"product_id":null};
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) { 

    this.imageForm=this._fb.group({
      colorimages_id:[0],
      image_name:[''],
      color_image:[''],
      image8:['']

    })
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    //       this._datacalls.getAllCategory().subscribe(
    //             data => {
    //               if (data.Data.length > 0) {
    //                 this.policiesList = data.Data[0].data;
    //                 console.log(this.policiesList);
    //               }
    //             }
    //           );

    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.product_id = param['product_id'];
        this.colorimages_id = param['colorimages_id'];
        // this.pid = param['repositorypdf_id'];
        //this.pid = param['repositorypdf_id'];
        console.log(this)
        if (this.product_id != undefined) {
          if (this.colorimages_id != undefined) {

            this._datacalls.getProduct(this.jsonPara).subscribe(

              data => {
                console.log('edit data:',data.Data.extra_data);
                for (var j = 0; j < data.Data.length; j++) {
                  for (var i = 0; i < data.Data[j].extra_data.length; i++) {

                    console.log(this.colorimages_id + " " + data.Data[j].extra_data[i].colorimages_id)
                    if (this.product_id == data.Data[j].product_id) {
                      if (this.colorimages_id == data.Data[j].extra_data[i].colorimages_id) {

                        this.imagepath8 = data.Data[j].extra_data[i].image_path;
                        console.log(data.Data[j].extra_data[i].image_path)

                        this.imageForm.patchValue({
                          'colorimages_id': data.Data[j].extra_data[i].colorimages_id,
                          'app_id': 13,

                          'product_id': this.product_id,

                          'image_path': data.Data[j].extra_data[i].image_path,



                        });

                        // this.filesToUpload = new Array<File>();

                      }
                    }
                  }
                }
              });
          }
        }
      });
console.log('og',this.product_id)


  }


  onChange(fileInput: any) {
    this.filesToUpload = fileInput.target.files['0'];
  console.log('......................')
 
 }

  onSubmit(){
debugger
   
 
    var form = new FormData();
    form.append("colorimages_id",this.imageForm.value.colorimages_id);
    form.append("app_id", "16");
    form.append("image_name",this.imageForm.value.image_name);
    form.append("color_name",this.imageForm.value.color_image);
    form.append("id",this.product_id);
    form.append("file1", this.filesToUpload);
      
      console.log('sfsd',form)
      this._datacalls.addcolorimages(form).subscribe(
        
        posts=>{

          if(posts['Success']==true){
            this.alertClass = "alert alert-success text-center alert-dismissible";
           this.errorMsg = 'Color Added Successfully';
             window.setTimeout(() => {
           this.Router.navigate(['colorimage']);
         }, 2000);
         
         } 
         else{
           this.alertClass = "alert alert-danger text-center alert-dismissible";
           this.errorMsg = 'Form not Filled properly';

         }
   
     console.log('Run succesfull');
       
 });


}

}       