import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addcolourstone',
  templateUrl: './addcolourstone.component.html',
  styleUrls: ['./addcolourstone.component.css'],
  providers:[datacalls]
})
export class AddcolourstoneComponent implements OnInit {

 public colourstoneForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
   // file_keys: string[];
    id; 
   // scheme_id;
    filesToUpload;
    //imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.colourstoneForm = this.fb.group({
            id:[0],
            app_id:[23],
            colour_stone_price: ['', [<any>Validators.required]],
             colour_stone: ['', [<any>Validators.required]],
              //start_date: ['', [<any>Validators.required]],
              //launched_date: ['', [<any>Validators.required]],
               

        });
     }


    ngOnInit() {

      this.id=this.activatedRoute.snapshot.params['id']
          console.log(this.id)
          if (this.id != undefined) {
  
            this._datacalls.getColourstone(this.id).subscribe(
              
              data => {
                              
              console.log( 'data',data.Data[0]);
              
                   
                  this.colourstoneForm.patchValue({
                    'id': data.Data[0].id,
                    'app_id':23,
                    'colour_stone_price': data.Data[0].colour_stone_price,
                    'colour_stone': data.Data[0].colour_stone,
                    // 'file': data.Data[0].file,
                    // 'start_date': data.Data[0].start_date,
                    // 'launched_date': data.Data[0].launched_date,
                    // 'guest': data.Data[0].guest,
                    });
                  //this.filesToUpload=new Array<File>();
                  //this.imagepath1= this.colourstoneForm.value.file;
                  //console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
   // onChange(fileInput: any) {
     // this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   //}



  onSubmit() {
     
    var formData = new FormData();
   
    formData.append("id", this.colourstoneForm.value.id);
    formData.append("app_id", this.colourstoneForm.value.app_id);
    formData.append("colour_stone_price", this.colourstoneForm.value.colour_stone_price);
    formData.append("colour_stone", this.colourstoneForm.value.colour_stone);
    
    formData.append("active_status",1);

    // console.log(encodeURI(this.colourstoneForm.value.colour));
    //  console.log("Encoded URI Component-->"+encodeURIComponent(this.colourstoneForm.value.colour));
    
    
 console.log('form',formData)
 
  this._datacalls.addColourstone(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.colourstoneForm.value.id==0) {
            this.errorMsg = 'Colourstone Added SuccessFully';
          } else {
            this.errorMsg = 'Colourstone Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['colourstone']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }