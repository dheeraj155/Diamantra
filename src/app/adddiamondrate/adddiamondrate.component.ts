import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adddiamondrate',
  templateUrl: './adddiamondrate.component.html',
  styleUrls: ['./adddiamondrate.component.css'],
  providers:[datacalls]
})
export class AdddiamondrateComponent implements OnInit {

  public diamondrateForm: FormGroup;
  public submitted: boolean;
 public events: any[] = [];
 shapelist: any;
 claritylist:any;
 sizelist:any;
 posts:any;
 imagepath: string = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
id;
clarity_id;
shape_id;
diamond_size_id;
 a: any;
 filesToPost: any;
 file_keys: string[];
 diamantra_diamond_rate_id;
 colour_name;
 size_2;
 alertClass: string;
 errorMsg: string;
 resized_images_list: { filename: string, url: string, dimension: string }[];
 isFileLoaded: boolean = false;
 isValidImage: boolean = true;

   constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

       this.diamondrateForm = this.fb.group({
        diamantra_diamond_rate_id:[0],
           app_id:[23],
          clarity: ['', [<any>Validators.required]],
          shape: ['', [<any>Validators.required]],
          size1: ['', [<any>Validators.required]],
          size2: [''],
          rate: ['', [<any>Validators.required]],
          colour: ['']
       });
    }


    changecolor(id){

 console.log('id'+ id);
      this._datacalls.getClarity(id).subscribe(posts => {
        this.colour_name=posts.Data[0].colour;
        this.clarity_id=posts.Data[0].id;
      });

    }

    changesizelist(shape_id){
      
       console.log('shape_id'+ shape_id);
            this._datacalls.getDiamondsize(shape_id,null).subscribe(posts => {
              this.sizelist=posts.Data;
              this.shape_id=posts.Data[0].shape_id;
              //this.diamond_size_id=posts.Data[0].diamond_size_id;
            });
      
          }

          changesize2(id){
            
             console.log('id'+ id);
                  this._datacalls.getDiamondsize(null,id).subscribe(posts => {
                    console.log('id'+ id);
                    this.size_2=posts.Data[0].size2;
                    this.diamond_size_id=posts.Data[0].diamond_size_id;
                  });
                }
            

ngOnInit() {
  this._datacalls.getClarity(null).subscribe(
    data => {
      if (data.Data.length > 0) {
        this.claritylist = data.Data;
        this.colour_name=' ';
        this.size_2=' ';
        console.log("clarity:" , this.claritylist);
      }
    }
  );
   
  this._datacalls.getDiamondsize(null,null).subscribe(posts => {
    this.sizelist=posts.Data;
    // this.shape_id=posts.Data[0].shape_id;
    //this.diamond_size_id=posts.Data[0].diamond_size_id;
  });

  this._datacalls.getShape().subscribe(
    data => {
      if (data.Data.length > 0) {
        this.shapelist = data.Data;
        console.log("shape:" , this.shapelist);
      }
    }
  );
 
  
   window.scrollTo(0,0);
   this.activatedRoute.params.subscribe(
     (param: any) => {

       this.diamantra_diamond_rate_id = param['id'];
       console.log(this)
       if (this.diamantra_diamond_rate_id != undefined) {
         this._datacalls.getDiamondrate(null,null,null,this.diamantra_diamond_rate_id).subscribe(

           data => {
             
            // console.log('edit data:', data.Data);
            //  for (var i = 0; i < data.Data.length; i++) {
            //    if (this.diamantra_diamond_rate_id == data.Data[i].diamantra_diamond_rate_id) {

                console.log('data:',data.Data)
                 //this.imagepath = data.Data[i].image_path;
                 //console.log('dataaas:',data.Data[i].image_path)
                 //console.log('edit data descrip:', data.Data[i].subject);
                 this.diamondrateForm.patchValue({
                   'diamantra_diamond_rate_id': data.Data[0].diamantra_diamond_rate_id,
                   'app_id':23,
                   'clarity': data.Data[0].clarity_id,
                   'colour': data.Data[0].colour,
                   'size1': data.Data[0].size_id,
                   //'size_id': data.Data[0].size_id,
                   'rate': data.Data[0].rate,
                   'size2': data.Data[0].size2,
                   'shape': data.Data[0].shape_id,
                   //'shape_id': data.Data[0].shape_id,
                   

                 });
                 console.log('clarity', data.Data[0].clarity_id)
                 console.log('size1', data.Data[0].size_id)
                 console.log('shape', data.Data[0].shape_id)
                 console.log('size_id:'+ this.diamondrateForm.value.size1)
                 this.filesToUpload = new Array<File>();
                
            //    }
            //  }
           }
         );
       }
     });


 }

 file: File;
 filesToUpload: Array<File>;

 _url: string = " http://myappcenter.co.in/testapi/api/insertdiamantradiamondrate";
 onChange(fileInput: any) {
   this.isFileLoaded = true;
   this.filesToUpload = <Array<File>>fileInput.target.files;
   let size = fileInput.target.files[0].size;
   console.log("fileupload: " , this.filesToUpload);
   console.log("Size: " + size);
   if (size >= 500000) {
     this.isValidImage = false;
     console.log("isValidImage: " + this.isValidImage);
   } else {
     this.isValidImage = true;
     console.log("isValidImage: " + this.isValidImage);
   }

 }

 onSubmit() {
   var formData: any = new FormData();
   
   formData.append("diamantra_diamond_rate_id", this.diamondrateForm.value.diamantra_diamond_rate_id);
   if(this.diamondrateForm.value.clarity!=undefined){
   formData.append("clarity_id",this.diamondrateForm.value.clarity );}
   else{formData.append("clarity_id", this.clarity_id);}
    //formData.append("colour", this.diamondrateForm.value.colour);
    if(this.diamondrateForm.value.clarity!=undefined){
      formData.append("shape_id",this.diamondrateForm.value.shape );}
      else{formData.append("shape_id", this.shape_id);}
    //formData.append("shape_id", this.shape_id);
    if(this.diamondrateForm.value.clarity!=undefined){
      formData.append("size_id",this.diamondrateForm.value.size1 );}
      else{formData.append("size_id", this.diamond_size_id);}
    //formData.append("size_id", this.diamond_size_id);
    //formData.append("size2", this.diamondrateForm.value.size2);
    formData.append("rate", this.diamondrateForm.value.rate);
   formData.append("app_id",23);
  
   console.log('formdata'+formData);
   this.makeFileRequest(this._url, formData, this.filesToUpload).then((result) => {
     console.log('result'+result);
     if (result['Success'] == true) {
       this.alertClass = "alert alert-success text-center  alert-dismissible";
       if (this.diamondrateForm.value.diamantra_diamond_rate_id==0) {
           this.errorMsg = 'Diamond Rate Added SuccessFully';
         } else {
           this.errorMsg = 'Diamond Rate Edited SuccessFully';
         }
       window.setTimeout(() => {
         this.Router.navigate(['diamondrate']);
       },1000);
     }
     else {
       this.alertClass = "alert alert-danger text-center  alert-dismissible";
       this.errorMsg = result['Message'];
     }

   }, (error) => {
     console.error(error);
   });
 }
 makeFileRequest(url: string, formData: FormData, files: Array<File>) {
   return new Promise((resolve, reject) => {
     var xhr = new XMLHttpRequest();
     // for (var i = 0; i < files.length; i++) {
     //   formData.append("file", files[i], files[i].name);
     // }

     xhr.onreadystatechange = function () {
       if (xhr.readyState == 4) {
         if (xhr.status == 200) {
           resolve(JSON.parse(xhr.response));
         } else {
           reject(xhr.response);
         }
       }
     }
     xhr.open("POST", url, true);
     xhr.send(formData);
   });
 }

}