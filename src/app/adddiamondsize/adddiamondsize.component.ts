import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adddiamondsize',
  templateUrl: './adddiamondsize.component.html',
  styleUrls: ['./adddiamondsize.component.css'],
  providers:[datacalls]
})
export class AdddiamondsizeComponent implements OnInit {

  public diamondsizeForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
   // file_keys: string[];
    id; 
   // scheme_id;
    filesToUpload;
    //imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    shape_id;
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.diamondsizeForm = this.fb.group({
            diamond_size_id:[0],
            app_id:[23],
            size2: ['', [<any>Validators.required]],
             size1: ['', [<any>Validators.required]],
              //start_date: ['', [<any>Validators.required]],
              //launched_date: ['', [<any>Validators.required]],
               

        });
     }


    ngOnInit() {
this.shape_id=this.activatedRoute.snapshot.params['shape_id']
      this.id=this.activatedRoute.snapshot.params['id']
          console.log(this.id)
          if (this.shape_id != undefined) {
            if (this.id != undefined) {
  
            this._datacalls.getDiamondsize(this.shape_id,this.id).subscribe(
              
              data => {
                              
              console.log( 'data',data.Data[0]);
              
                   
                  this.diamondsizeForm.patchValue({
                    'diamond_size_id': data.Data[0].diamond_size_id,
                    'app_id':23,
                    //'clarity': data.Data[0].clarity,
                    'size1': data.Data[0].size1,
                    'size2': data.Data[0].size2,
                    'shape_id':this.shape_id,
                    // 'start_date': data.Data[0].start_date,
                    // 'launched_date': data.Data[0].launched_date,
                    // 'guest': data.Data[0].guest,
                    });
                  //this.filesToUpload=new Array<File>();
                  //this.imagepath1= this.diamondsizeForm.value.file;
                  //console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          }  
  
    }
   // onChange(fileInput: any) {
     // this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   //}



  onSubmit() {
     
    var formData = new FormData();
   
    formData.append("diamond_size_id", this.diamondsizeForm.value.diamond_size_id);
    formData.append("app_id", this.diamondsizeForm.value.app_id);
    formData.append("size2", this.diamondsizeForm.value.size2);
    formData.append("size1", this.diamondsizeForm.value.size1);
    
    formData.append("shape_id",this.shape_id);

    // console.log(encodeURI(this.diamondsizeForm.value.colour));
    //  console.log("Encoded URI Component-->"+encodeURIComponent(this.diamondsizeForm.value.colour));
    
    
 console.log('form',formData)
 
  this._datacalls.addDiamondsize(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.diamondsizeForm.value.id==0) {
            this.errorMsg = 'Diamondsize Added SuccessFully';
          } else {
            this.errorMsg = 'Diamondsize Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['diamondsize/'+this.shape_id]);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }