import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adddiamondweight',
  templateUrl: './adddiamondweight.component.html',
  styleUrls: ['./adddiamondweight.component.css'],
  providers:[datacalls]
})
export class AdddiamondweightComponent implements OnInit {

 
public diamondweightForm: FormGroup;
public submitted: boolean;
public events: any[] = [];
a:any;
filesToPost: any;
   alertClass: string;
errorMsg: string;
// file_keys: string[];
id; 
// scheme_id;
filesToUpload;
//imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
isFileLoaded:boolean = false;
isValidImage:boolean = true;
shape_id;
reweightd_images_list: { filename: string, url: string, dimension: string }[];

constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

    this.diamondweightForm = this.fb.group({
        diamantra_weight_id:[0],
        app_id:[23],
        weight: ['', [<any>Validators.required]],
         size: ['', [<any>Validators.required]],
          //start_date: ['', [<any>Validators.required]],
          //launched_date: ['', [<any>Validators.required]],
           

    });
 }


ngOnInit() {
this.shape_id=this.activatedRoute.snapshot.params['shape_id']
  this.id=this.activatedRoute.snapshot.params['id']
      console.log(this.id)
      if (this.shape_id != undefined) {
        if (this.id != undefined) {

        this._datacalls.getDiamondweight(this.shape_id,this.id).subscribe(
          
          data => {
                          
          console.log( 'data',data.Data[0]);
          
               
              this.diamondweightForm.patchValue({
                'diamantra_weight_id': data.Data[0].diamantra_weight_id,
                'app_id':23,
                //'clarity': data.Data[0].clarity,
                'size': data.Data[0].size,
                'weight': data.Data[0].weight,
                'shape_id':this.shape_id,
                // 'start_date': data.Data[0].start_date,
                // 'launched_date': data.Data[0].launched_date,
                // 'guest': data.Data[0].guest,
                });
              //this.filesToUpload=new Array<File>();
              //this.imagepath1= this.diamondweightForm.value.file;
              //console.log('path',this.imagepath1); 
              
                            }
            
          
        );
      }
    // });
      }  

}
// onChange(fileInput: any) {
 // this.filesToUpload = fileInput.target.files['0'];
//  this.isFileLoaded = true;

//}



onSubmit() {
 
var formData = new FormData();

formData.append("diamantra_weight_id", this.diamondweightForm.value.diamantra_weight_id);
formData.append("app_id", this.diamondweightForm.value.app_id);
formData.append("weight", this.diamondweightForm.value.weight);
formData.append("size", this.diamondweightForm.value.size);

formData.append("shape_id",this.shape_id);

// console.log(encodeURI(this.diamondweightForm.value.colour));
//  console.log("Encoded URI Component-->"+encodeURIComponent(this.diamondweightForm.value.colour));


console.log('form',formData)

this._datacalls.addDiamondweight(formData).subscribe(posts => {
   
  if(posts['Success']==true){
          this.alertClass = "alert alert-success text-center alert-dismissible";
           if (this.diamondweightForm.value.diamantra_weight_id==0) {
        this.errorMsg = 'Diamondweight Added SuccessFully';
      } else {
        this.errorMsg = 'Diamondweight Edited SuccessFully';
      }
           window.setTimeout(() => {
         this.Router.navigate(['diamondweight/'+this.shape_id]);
       }, 2000);
       
       } 
       else{
         this.alertClass = "alert alert-danger text-center alert-dismissible";
         this.errorMsg = 'Form not Filled properly';

       }
 
   console.log('Run succesfull');
     
});

}
}