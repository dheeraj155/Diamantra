import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addimageslider',
  templateUrl: './addimageslider.component.html',
  styleUrls: ['./addimageslider.component.css'],
    providers: [datacalls]
})
export class AddimagesliderComponent implements OnInit {

  public imagesliderForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];
 a:any;
  filesToPost: any;
     alertClass: string;
 errorMsg: string;
  file_keys: string[];
  id; 
  image_slider_id;
  filesToUpload;
  imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
  isFileLoaded:boolean = false;
  isValidImage:boolean = true;
  
  resized_images_list: { filename: string, url: string, dimension: string }[];

  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

      this.imagesliderForm = this.fb.group({
          image_slider_id:[0],
          app_id:[23],
          image_name: ['', [<any>Validators.required]],            
          file:[''],
          image_path:[''],
          //type:['', [<any>Validators.required]],

      });
   }


  ngOnInit() {

console.log('hello')

    // this.activatedRoute.params.subscribe(
    //   (param: any) => {

    //     this.image_slider_id = param['image_slider_id'];

    this.image_slider_id=this.activatedRoute.snapshot.params['image_slider_id']
        console.log(this.image_slider_id)
        if (this.image_slider_id != undefined) {

          this._datacalls.getImageSliders(this.image_slider_id).subscribe(
            
            data => {
                            
                            console.log( 'data',data.Data[0]);

                 this.imagepath1=data.Data[0].image_path,             
                 
                this.imagesliderForm.patchValue({
                  'image_slider_id': data.Data[0].image_slider_id,
                  'app_id': 23,
                  'image_name': data.Data[0].image_name,
                  //'description': data.Data[0].description,
                  //'location': data.Data[0].location,
                  'image_path': data.Data[0].image_path,
                 // 'type': data.Data[0].type,
                  });
                this.filesToUpload=new Array<File>();
                this.imagepath1= this.imagesliderForm.value.image_path;
                console.log('path',this.imagepath1); 
                
                              }
              
            
          );
        }
      // });
        

  }
  onChange(fileInput: any) {
    this.filesToUpload = fileInput.target.files['0'];
  //  this.isFileLoaded = true;
 
 }



onSubmit() {
   
  var formData = new FormData();
 
  formData.append("image_slider_id", this.imagesliderForm.value.image_slider_id);
  formData.append("app_id", this.imagesliderForm.value.app_id);
  formData.append("image_name", this.imagesliderForm.value.image_name);
  formData.append("active_status",1);
  formData.append("image_description", this.imagesliderForm.value.image_path.mimetype);
  formData.append("file1", this.filesToUpload);
  formData.append("created_by",23);
  formData.append("imagepath1",this.imagepath1);
  //formData.append("gaon_id",1);
  
  
console.log('form',formData)

this._datacalls.addImageSlider(formData).subscribe(posts => {
     
    if(posts['Success']==true){
            this.alertClass = "alert alert-success text-center alert-dismissible";
             if (this.imagesliderForm.value.image_slider_id==0) {
          this.errorMsg = 'Imageslider Added SuccessFully';
        } else {
          this.errorMsg = 'Imageslider Edited SuccessFully';
        }
             window.setTimeout(() => {
           this.Router.navigate(['imageslider']);
         }, 2000);
         
         } 
         else{
           this.alertClass = "alert alert-danger text-center alert-dismissible";
           this.errorMsg = 'Form not Filled properly';

         }
   
     console.log('Run succesfull');
       
 });

}
}
