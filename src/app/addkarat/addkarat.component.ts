import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addkarat',
  templateUrl: './addkarat.component.html',
  styleUrls: ['./addkarat.component.css'],
  providers:[datacalls]
})
export class AddkaratComponent implements OnInit {

  public karatForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
   // file_keys: string[];
    id; 
   // scheme_id;
    filesToUpload;
    //imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.karatForm = this.fb.group({
            id:[0],
            app_id:[23],
            price: ['', [<any>Validators.required]],
             karat: ['', [<any>Validators.required]],
            making_charges: ['', [<any>Validators.required]],
              //launched_date: ['', [<any>Validators.required]],
               

        });
     }


    ngOnInit() {

      this.id=this.activatedRoute.snapshot.params['id']
          console.log(this.id)
          if (this.id != undefined) {
  
            this._datacalls.getKarat(this.id).subscribe(
              
              data => {
                              
              console.log( 'data',data.Data[0]);
              
                   
                  this.karatForm.patchValue({
                    'id': data.Data[0].id,
                    'app_id':23,
                    'price': data.Data[0].price,
                    'karat': data.Data[0].karat,
                    // 'file': data.Data[0].file,
                    'making_charges': data.Data[0].making_charges,
                    // 'launched_date': data.Data[0].launched_date,
                    // 'guest': data.Data[0].guest,
                    });
                  //this.filesToUpload=new Array<File>();
                  //this.imagepath1= this.karatForm.value.file;
                  //console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
   // onChange(fileInput: any) {
     // this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   //}



  onSubmit() {
   
    var formData = new FormData();
   
    formData.append("id", this.karatForm.value.id);
    formData.append("app_id", this.karatForm.value.app_id);
    formData.append("price", this.karatForm.value.price);
    formData.append("karat", this.karatForm.value.karat);
    
    // if(this.imagepath1 != undefined){
    // formData.append("file", this.filesToUpload);
    // }

    // if(this.imagepath1 == undefined ){
    //   formData.append("file", this.imagepath1);
    // }

    formData.append("making_charges", this.karatForm.value.making_charges);
    // formData.append("launched_date", this.karatForm.value.launched_date);
    // formData.append("guest", this.karatForm.value.guest);
    //  formData.append("location",'NA');
    //formData.append("active_status",1);

    // console.log(encodeURI(this.karatForm.value.colour));
    //  console.log("Encoded URI Component-->"+encodeURIComponent(this.karatForm.value.colour));
    
    
 console.log('form',formData)
 
  this._datacalls.addKarat(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
               if (this.karatForm.value.id==0) {
            this.errorMsg = 'Karat Added SuccessFully';
          } else {
            this.errorMsg = 'Karat Edited SuccessFully';
          }
               window.setTimeout(() => {
             this.Router.navigate(['karat']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }