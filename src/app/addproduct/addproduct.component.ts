import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';

import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router'
@Component({
    selector: 'app-addproduct',
    templateUrl: './addproduct.component.html',
    styleUrls: ['./addproduct.component.css'],
    providers: [datacalls]
})
export class AddproductComponent implements OnInit {
    public productForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    colorlist:any;
    claritylist:any;
    shapelist:any;
    karatlist:any;
   alertClass: string;
   errorMsg: string;
   filesToUpload;
   imagepath1;
weightlist:any;
   product_id; clarity_id;
   diamantra_weight_id; total_making_charges;
   diamond_size_id; single_diamond_price; new_estimated_price; 
   diamond_rate_value; karat_price; colourstone_price;
   diamond_price; gold_price; 
   id:any; making_charges; total_gold_price;
   calculated_product_price=0; tax; estimated_price;
   size:any; weight:any;
   shape_id:any;
   subcat_id;
   isproduct:boolean;
    imagepath:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath2:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath3:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath4:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    imagepath5:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    public uploader: FileUploader = new FileUploader({ url: '35.154.141.107:4000/api/insertproduct' });
     a:any;
    filesToPost: any;
    data;
    file_keys: string[];
    
    filesToUpload1;
    filesToUpload2;
    filesToUpload3;
    filesToUpload4;
    filesToUpload5;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];
    isFileLoaded:boolean = false;
    jsonPara = {};
    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.productForm = this.fb.group({
            product_id:[0],
            app_id:[23],
            product_name: ['', Validators.required],
            product_code: ['',Validators.required],
            diamond_weight:[''],
            description:[''],
            discount_percentage:[''],
            colour_stone:[''],
            gold_weight:[''],
            discount_price:[''],
            image1:[''],
            shape:[''],
            karat:[''],
            clarity:[''],
            diamond_rate:[''],
            image2:[''],
            image3:[''],
            image4:[''],
            image5:['']
        });
     }
    

//     onChangeDiamondNumbers(discount_price){
//       console.log('discount_price '+discount_price);
//     this.diamond_price= this.single_diamond_price * this.productForm.value.discount_price;
//     console.log('diamond_price '+ this.diamond_price);
//     }
    
//     onChangeGoldWeight(gold_weight){
      
//       console.log('this.karat_price '+this.karat_price);
//       console.log('this.making_charges '+this.making_charges);
//       console.log('this.colourstone_price '+this.colourstone_price);
//       // console.log('gold_weight '+gold_weight);
//       // console.log('gold_weight '+gold_weight);
//     this.gold_price= (this.karat_price * this.productForm.value.gold_weight);
//     this.total_making_charges= (this.making_charges * this.productForm.value.gold_weight);
//   this.total_gold_price= (this.gold_price + this.total_making_charges);
//   this.estimated_price= (this.diamond_price + this.total_gold_price); 

//   if(this.colourstone_price==undefined || this.colourstone_price==null){
//  this.new_estimated_price=parseInt(this.estimated_price) + 0;  
//   } else {
//     this.new_estimated_price= parseInt(this.estimated_price) + parseInt(this.colourstone_price);   
//   }

//   this.tax= (this.new_estimated_price * 0.18);

//   this.calculated_product_price= (this.new_estimated_price + this.tax);
  
//   console.log('gold_price '+ this.gold_price);
//   console.log('total_making_charges '+ this.total_making_charges);
//   console.log('total_gold_price '+ this.total_gold_price);
//   console.log('estimated_price '+ this.estimated_price);
//   console.log('new_estimated_price '+ this.new_estimated_price);
//   console.log('tax '+ this.tax);
//   console.log('calculated_product_price '+ this.calculated_product_price);

// }

// onChangeKarat(id){
  
//   this._datacalls.getKarat(id).subscribe(
//     data => { 
//       if (data.Data.length > 0) {
//         this.karat_price=data.Data[0].price;
//         this.making_charges= data.Data[0].making_charges;
//         console.log("karat_price:" , this.karat_price);
//         console.log("making_charges:" , this.making_charges);
//       }
//     }
//   );
// }

// onChangeColorStone(id){
  
//   this._datacalls.getColourstone(id).subscribe(
//     data => {
//       if (data.Data.length > 0) {
//         this.colourstone_price=data.Data[0].colour_stone_price;
//         console.log("colourstone_price:" , this.colourstone_price);
//       }
//     }
//   );
// }

    ngOnInit() {
      this._datacalls.getClarity(null).subscribe(
        data => {
          if (data.Data.length > 0) {
            this.claritylist = data.Data;
            console.log("clarity:" , this.claritylist);
            this.clarity_id=this.productForm.value.clarity;
            console.log("clarity_id: ", this.clarity_id)
          }
        }
      );

      this._datacalls.getKarat(null).subscribe(
        data => { 
          if (data.Data.length > 0) {
            this.karatlist = data.Data;
          }
        }
      );

      this._datacalls.getShape().subscribe(
        data => {
          if (data.Data.length > 0) {
            this.shapelist = data.Data;
            console.log("shape:" , this.shapelist);
          }
        }
      );
    
      this._datacalls.getColourstone(null).subscribe(
        data => {
          if (data.Data.length > 0) {
            this.colorlist = data.Data;
            // this.colourstone_price=data.Data[0].colour_stone_price;
            // console.log("Colourstone:" , this.colorlist);
            // console.log("colourstone_price:" , this.colourstone_price);
          }
        }
      );

      this._datacalls.getDiamondweight(null,null).subscribe(data => {
        this.weightlist=data.Data;
      })

      console.log('value of diamond_rate while not editing:'+this.productForm.value.diamond_rate)

      // this.diamond_price= this.diamond_rate * this.weight * this.productForm.value.discount_price;
      // this.gold_price= (this.karat_price * this.productForm.value.gold_weight) * this.making_charges;
      // this.calculated_product_price= (this.diamond_price + this.gold_price + this.colourstone_price) * 18/100
      this.activatedRoute.params.subscribe(
        (param: any) => {
          this.id = param['id'];
          this.subcat_id = param['subcat_id'];
          this.product_id = param['product_id'];
          console.log(this.id)

          var json={

            "app_id":23,"category_id":this.id 
            ,"sub_category_id":this.subcat_id,"id":this.product_id
            
            
          }
          if (this.product_id != undefined) {
  
            this._datacalls.getProduct(json).subscribe(
              
              data => {   
                this.imagepath1=data.Data[0].image1;
                this.imagepath2=data.Data[0].extra_image.image2;
                this.imagepath3=data.Data[0].extra_image.image3;
                this.imagepath4=data.Data[0].extra_image.image4;
                this.imagepath5=data.Data[0].extra_image.image5;
                console.log("Image while editing: ", data.Data[0].image1) 
                console.log("data editing: ", data.Data)       
                  this.productForm = this._fb.group({
                   "product_id":data.Data[0].product_id,
                    "app_id": "23",
                    "product_name": data.Data[0].product_name,
                    "product_code":data.Data[0].product_code ,
                    "category_id":this.id,
                    "sub_category_id":this.subcat_id,
                    "description": data.Data[0].description,
                    "diamond_weight": data.Data[0].diamantra_weight_id,
                    "colour_stone":data.Data[0].colour_stone_id,
                    "shape":data.Data[0].shape_id,
                    "active_status":'1' ,      
                    "karat":data.Data[0].karat_id,
                    "clarity":data.Data[0].clarity_id,
                    "discount_price": data.Data[0].discount_price,
                    "gold_weight":data.Data[0].gold_weight,
                    "image1":data.Data[0].image1,
                    "discount_percentage":data.Data[0].discount_percentage,
                    "diamond_rate":data.Data[0].diamond_rate,
                    "image2":data.Data[0].extra_image.imagepath2,
                    "image3":data.Data[0].extra_image.imagepath3,
                    "image4":data.Data[0].extra_image.imagepath4,
                    "image5":data.Data[0].extra_image.imagepath5,
                    
                    
                    
                  });
                  this.filesToUpload=new Array<File>();
                  console.log('value of diamond_rate:'+this.productForm.value.diamond_rate)
                  //this.imagepath1= this.productForm.value.image1;
                  
                  //console.log('Shape',this.productForm.value.shape);
                  //this.filesToUpload1=new Array<File>();
                   }
            );
          }
        });
    }
  
    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }
   onChange1(fileInput1: any) {
    this.filesToUpload1= fileInput1.target.files['0'];
  //  this.isFileLoaded = true;
 
 }
onChange2(fileInput2: any) {
this.filesToUpload2 = fileInput2.target.files['0'];
//  this.isFileLoaded = true;

}
onChange3(fileInput3: any) {
this.filesToUpload3 = fileInput3.target.files['0'];
//  this.isFileLoaded = true;

}
onChange4(fileInput4: any) {
this.filesToUpload4 = fileInput4.target.files['0'];
//  this.isFileLoaded = true;

}

onChangeShape(shape_id){
  console.log('shape_id '+shape_id);
this._datacalls.getDiamondweight(shape_id,null).subscribe(data => {
   this.weightlist=data.Data;
    // this.diamantra_weight_id=data.Data[0].diamantra_weight_id;
     this.shape_id=data.Data[0].shape_id;       
 });
}

onChangeWeight(diamantra_weight_id){
  console.log('shape:',this.shape_id );
  this._datacalls.getDiamondweight(this.shape_id,diamantra_weight_id).subscribe(data => {
     this.diamantra_weight_id=data.Data[0].diamantra_weight_id;
     //this.shape_id=data.Data[0].shape_id;
     this.size=data.Data[0].size;
     this.weight=data.Data[0].weight;
     console.log('Weight_id '+ this.diamantra_weight_id);
     console.log('size '+ this.size);
     console.log('weight '+ this.weight);
    
  
this._datacalls.getDiamondsize(this.shape_id,null).subscribe(data => {
  for(var i=0;i<data.Data.length;i++){
   if(this.size>=data.Data[i].size1 && this.size<=data.Data[i].size2){
    this.diamond_size_id=data.Data[i].diamond_size_id;
    console.log('diamond_size_id'+ this.diamond_size_id);

    this._datacalls.getDiamondrate(this.productForm.value.clarity,this.shape_id,this.diamond_size_id,null).subscribe(
      data => {
        this.diamond_rate_value=data.Data[0].rate;
        //this.single_diamond_price= this.diamond_rate * this.weight;
        // this.calculated_product_price=this.single_diamond_price; 
        console.log('diamond_rate',this.diamond_rate_value);
        //console.log('single_diamond_price',this.single_diamond_price);
      }
    );
   }}
 });
});
}

    onSubmit(){
 
      var form = new FormData();
      debugger;
      form.append("product_id",this.productForm.value.product_id);
      form.append("app_id", "23");
      form.append("product_name", this.productForm.value.product_name);
      form.append("product_code",this.productForm.value.product_code );
      form.append("category_id",this.id);
      form.append("sub_category_id",this.subcat_id);
      form.append("description", this.productForm.value.description);

      if(this.productForm.value.diamond_weight!=undefined){
        form.append("diamantra_weight_id",this.productForm.value.diamond_weight );}
        else{form.append("diamantra_weight_id", this.diamantra_weight_id);}
      //form.append("diamantra_weight_id",this.productForm.value.diamond_weight);

      //form.append("file1",this.filesToUpload);
      form.append("active_status",'1');      
      form.append("offer",'');
      form.append("discount_price", this.productForm.value.discount_price);
      form.append("colour_stone_id",this.productForm.value.colour_stone);
      //form.append("file2",this.filesToUpload1);
      form.append("gold_weight",this.productForm.value.gold_weight);
      form.append("discount_percentage",this.productForm.value.discount_percentage);
      //form.append("imagepath1",this.imagepath1);
      if(this.productForm.value.shape!=undefined){
        form.append("shape_id",this.productForm.value.shape );}
        else{form.append("shape_id", this.shape_id);}

      // form.append("shape_id",this.productForm.value.shape);
      form.append("karat_id",this.productForm.value.karat);

      if(this.productForm.value.clarity!=undefined){
        form.append("clarity_id",this.productForm.value.clarity );}
        else{form.append("clarity_id", this.clarity_id);}
        
     form.append("product_price",'0');
     if(this.diamond_rate_value!=undefined){
     form.append("diamond_rate",this.diamond_rate_value) 
     }
     else{
      form.append("diamond_rate",this.productForm.value.diamond_rate)

     }

    //  if(this.productForm.value.diamond_rate!=null){
    //   form.append("diamond_rate",this.productForm.value.diamond_rate );
      
    //  } else{     
    //   form.append("diamond_rate",this.diamond_rate_value) 
    //   ;}
     
        //console.log("diamond_rate......",this.diamond_rate_value );
        
        form.append("imagepath1",this.imagepath1);
        form.append("file1",this.filesToUpload);
      
  
  
        form.append("imagepath2",this.imagepath2);
        form.append("file2",this.filesToUpload1);
      
  
        form.append("imagepath3",this.imagepath3);
        form.append("file3",this.filesToUpload2);
      
  
        form.append("imagepath4",this.imagepath4);
        form.append("file4",this.filesToUpload3);
        
      
  
      
        form.append("imagepath5",this.imagepath5);
        form.append("file5",this.filesToUpload4);
    
  
    
      console.log("formdata: " , this.filesToUpload)
  
     //form.append("diamond_rate",this.diamond_rate);
      
      
   
   console.log('form',form)
   
    this._datacalls.addProduct(form).subscribe(posts => {
         
        if(posts['Success']==true){
                this.alertClass = "alert alert-success text-center alert-dismissible";
               this.errorMsg = 'Product Added SuccessFully';
                 window.setTimeout(() => {
               this.Router.navigate(['productlist/'+this.id+'/'+this.subcat_id]);
             }, 2000);
             
             } 
             else{
               this.alertClass = "alert alert-danger text-center alert-dismissible";
               this.errorMsg = 'Form not Filled properly';
   
             }
       
         console.log('Run succesfull');
           
     });
   
   
   }
  }
  
export interface CategoryData {
    name: string,
    code:string,
    category:string,
    description:string,
    price:number,
    size:string
} 
