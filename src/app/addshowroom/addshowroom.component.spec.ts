import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddshowroomComponent } from './addshowroom.component';

describe('AddshowroomComponent', () => {
  let component: AddshowroomComponent;
  let fixture: ComponentFixture<AddshowroomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddshowroomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddshowroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
