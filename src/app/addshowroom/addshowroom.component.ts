import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addshowroom',
  templateUrl: './addshowroom.component.html',
  styleUrls: ['./addshowroom.component.css'],
  providers:[datacalls]
})
export class AddshowroomComponent implements OnInit {
  showroomForm;
  filesToUpload;
  alertClass;
  errorMsg;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router) { 

    this.showroomForm=this._fb.group({
      dickson_showroom_id:[0],
      app_id:[16],
      showroom_name: ['', Validators.required],
      address: ['',Validators.required],
      pincode:['',Validators.required],
      image1:['']
      

    });
  }

  ngOnInit() {
  }

  onChange(fileInput: any) {
    this.filesToUpload = fileInput.target.files['0'];
  //  this.isFileLoaded = true;
 
 }

  onSubmit(){

    var form = new FormData();
    form.append("dickson_showroom_id",this.showroomForm.value.dickson_showroom_id);
    form.append("app_id", "16");
    form.append("showroom_name", this.showroomForm.value.showroom_name);
    form.append("address",this.showroomForm.value.address );
    form.append("pincode",this.showroomForm.value.pincode);
    form.append("file1",this.filesToUpload);
    
 
 console.log('form',form)
 
  this._datacalls.addShowroom(form).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
             this.errorMsg = 'Showroom Added SuccessFully';
               window.setTimeout(() => {
             this.Router.navigate(['showroom']);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 


  }

}
