import { Component, OnInit, ChangeDetectorRef   } from '@angular/core';
import {FormGroup,FormBuilder,FormControl,Validators} from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addsubcategory',
  templateUrl: './addsubcategory.component.html',
  styleUrls: ['./addsubcategory.component.css'],
  providers:[datacalls]
})
export class AddsubcategoryComponent implements OnInit {

  public subcategoryForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
   a:any;
    filesToPost: any;
       alertClass: string;
   errorMsg: string;
    file_keys: string[];
    id; 
    subcat_id;
    imagePath1;
    filesToUpload;
    imagepath1:string="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
    isFileLoaded:boolean = false;
    isValidImage:boolean = true;
    
    resized_images_list: { filename: string, url: string, dimension: string }[];

    constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {

        this.subcategoryForm = this.fb.group({
            arya_category_id:[0],
            //app_id:[23],
            category_name: ['', [<any>Validators.required, <any>Validators.minLength(3)]],
            file:[''],
            image_path:['']

        });
     }


    ngOnInit() {

  console.log('hello')
  debugger
      // this.activatedRoute.params.subscribe(
      //   (param: any) => {
  
      //     this.image_slider_id = param['image_slider_id'];
 this.id=this.activatedRoute.snapshot.params['id'],
      this.subcat_id=this.activatedRoute.snapshot.params['subcat_id'],
     
          console.log(this)
          

  if (this.id != undefined) {
if (this.subcat_id != undefined) {
            this._datacalls.getSubcatid(this.id,this.subcat_id).subscribe(
              
              data => {
                              
                               console.log( 'data',data.Data[0]);
  
                                
                   
                  this.subcategoryForm = this._fb.group({
                    'arya_category_id': data.Data[0].arya_category_id,
                      'app_id': 23,
                    'category_name': data.Data[0].category_name,
                    'app_image': data.Data[0].app_image
                    });
                  this.filesToUpload=new Array<File>();
                  this.imagePath1= data.Data[0].app_image;
                  console.log('path',this.imagepath1); 
                  
                                }
                
              
            );
          }
        // });
          
  
    }
    }
 

  

    

    onChange(fileInput: any) {
      this.filesToUpload = fileInput.target.files['0'];
    //  this.isFileLoaded = true;
   
   }



  onSubmit() {
     
    var formData = new FormData();

   this.id=this.activatedRoute.snapshot.params['id'];
   console.log('parent_id'+this.id)
    formData.append("arya_category_id", this.subcategoryForm.value.arya_category_id);
    formData.append("app_id", '23');
    formData.append("category_name", this.subcategoryForm.value.category_name);
    formData.append("file",this.filesToUpload);
    formData.append("image1",this.imagePath1);
    // formData.append("file2",'http://13.126.122.152:6001//images/1507640754234no_photo_icon.jpg');
    formData.append("parent_id", this.id);
 
 console.log('form',formData)
 
  this._datacalls.addCategory(formData).subscribe(posts => {
       
      if(posts['Success']==true){
              this.alertClass = "alert alert-success text-center alert-dismissible";
             this.errorMsg = 'Category Added SuccessFully';
               window.setTimeout(() => {
             this.Router.navigate(['subcategory',this.id]);
           }, 2000);
           
           } 
           else{
             this.alertClass = "alert alert-danger text-center alert-dismissible";
             this.errorMsg = 'Form not Filled properly';
 
           }
     
       console.log('Run succesfull');
         
   });
 
  }
  }


