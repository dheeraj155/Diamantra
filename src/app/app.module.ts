import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
// import {DataTableModule} from 'angular2-datatable
import { DataTablesModule } from 'angular-datatables';


import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent} from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { PublicComponent} from './public/public.component';
import {SecureComponent} from './secure/secure.component';
import {  routing, appRoutingProviders } from './app.routing';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CategoryComponent } from './category/category.component';
import { AddcategoryComponent } from './addcategory/addcategory.component';
import { OrderComponent } from './order/order.component';
import{AuthgaurdGuard} from './authgaurd.guard';
import { ProductComponent } from './product/product.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ImagesliderComponent } from './imageslider/imageslider.component';
import { AddimagesliderComponent } from './addimageslider/addimageslider.component';
import { AddbrochuresComponent } from './addbrochures/addbrochures.component';
import { BrochuresComponent } from './brochures/brochures.component';
import { ShowroomComponent } from './showroom/showroom.component';
import { AddshowroomComponent } from './addshowroom/addshowroom.component';
import { MessageComponent } from './message/message.component';
import { QueryComponent } from './query/query.component';
import { ChatdetailsComponent } from './chatdetails/chatdetails.component';
import { AnswerComponent } from './answer/answer.component';
import { ArchitectComponent } from './architect/architect.component';
import { AddarchitectComponent } from './addarchitect/addarchitect.component';
import { ProductimageComponent } from './productimage/productimage.component';
import { ColorimagesComponent } from './colorimages/colorimages.component';
import { AddcolorimagesComponent } from './addcolorimages/addcolorimages.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { AddsubcategoryComponent } from './addsubcategory/addsubcategory.component';
import { ShapeComponent } from './shape/shape.component';
import { AddshapeComponent } from './addshape/addshape.component';
import { ClarityComponent } from './clarity/clarity.component';
import { AddclarityComponent } from './addclarity/addclarity.component';
import { KaratComponent } from './karat/karat.component';
import { AddkaratComponent } from './addkarat/addkarat.component';
import { ColourstoneComponent } from './colourstone/colourstone.component';
import { AddcolourstoneComponent } from './addcolourstone/addcolourstone.component';
import { DiamondsizeComponent } from './diamondsize/diamondsize.component';

import { AdddiamondsizeComponent } from './adddiamondsize/adddiamondsize.component';
import { DiamondweightComponent } from './diamondweight/diamondweight.component';
import { AdddiamondweightComponent } from './adddiamondweight/adddiamondweight.component';
import { DiamondrateComponent } from './diamondrate/diamondrate.component';
import { AdddiamondrateComponent } from './adddiamondrate/adddiamondrate.component';






@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    PublicComponent,
    SecureComponent,
    SidebarComponent,
    CategoryComponent,
    AddcategoryComponent,
    OrderComponent,
    ProductComponent,
    AddproductComponent,
    FileSelectDirective,
    MenuComponent,
    ImagesliderComponent,
    AddimagesliderComponent,
    AddbrochuresComponent,
    BrochuresComponent,
    ShowroomComponent,
    AddshowroomComponent,
    MessageComponent,
    QueryComponent,
    ChatdetailsComponent,
    AnswerComponent,
    ArchitectComponent,
    AddarchitectComponent,
    ProductimageComponent,
    ColorimagesComponent,
    AddcolorimagesComponent,
    SubcategoryComponent,
    AddsubcategoryComponent,
    ShapeComponent,
    AddshapeComponent,
    ClarityComponent,
    AddclarityComponent,
    KaratComponent,
    AddkaratComponent,
    ColourstoneComponent,
    AddcolourstoneComponent,
    DiamondsizeComponent,
   
    AdddiamondsizeComponent,
   
    DiamondweightComponent,
   
    AdddiamondweightComponent,
   
    DiamondrateComponent,
   
    AdddiamondrateComponent

    
    
  ],
  imports: [
    BrowserModule,
    
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
    DataTablesModule
    
   
  ],
  providers: [appRoutingProviders, CookieService,AuthgaurdGuard,  Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
