import { Component, OnInit, ElementRef  } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';

@Component({
  selector: 'app-architect',
  templateUrl: './architect.component.html',
  styleUrls: ['./architect.component.css'],
  providers:[datacalls]
})
export class ArchitectComponent implements OnInit {
  posts;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  value;
  value1;
    constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) { }
  
  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    this._DatacallsService.getuser('architect',null).subscribe(posts => {
      this.posts=posts.Data;
        console.log('a:',this.posts);
        console.log('data',this.posts.length)
        this.dtTrigger.next();
       console.log(this.posts);
       this.value=this.posts.length
       console.log('iinside'+this.value) 
    });
   
  }

  
}
