import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot ,Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthgaurdGuard implements CanActivate {

  constructor(private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if(sessionStorage.getItem('currentuser')==undefined){
        this.router.navigate(['/login'])
        return false;
              }
        else {
        return true;
              }
    
  }
}
