import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';

@Component({
  selector: 'app-brochures',
  templateUrl: './brochures.component.html',
  styleUrls: ['./brochures.component.css'],
  providers:[datacalls]
})
export class BrochuresComponent implements OnInit {
posts;
dtOptions: DataTables.Settings = {};
dtTrigger: Subject<any>= new Subject();
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) { }

  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    this._DatacallsService.getBrochures(null).subscribe(posts => {
      this.posts=posts.Data;
        console.log('a:',this.posts);
        this.dtTrigger.next();
       console.log(this.posts);
    });
   

  }

}
