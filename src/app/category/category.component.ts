import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { ActivatedRoute,Router} from '@angular/router';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
   providers:[datacalls],
})
export class CategoryComponent implements OnInit {
 tableWidget: any = {};
 posts:Post[];
  a: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  parentId;
  id;
  isSubcat: boolean;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef,private activatedRoute:ActivatedRoute, private Router:Router) {
  
   // console.log('b');
  
    
   
  }
ngAfterViewInit(){
  this.initDatatable();
}

private initDatatable(): void {
    // debugger
    // let exampleId: any = $('#example');
    // this.tableWidget = exampleId.DataTable({
    //   select: true
    // });
 
  }

  // change(catId,type){
  //   this._DatacallsService.changeDisplayType(catId,type).subscribe(posts => {
  //       //  window.setTimeout(() => {
  //       //     this.Router.navigate(['category']);
  //       //     //  location.reload();
  //       //   });
        
  //       this.ngOnInit()

  //  // console.log(this.posts);
  // });
  // }

  ngOnInit() {
   // console.log('a');

  //  this.activatedRoute.params.subscribe(
  //     (param: any) => {

  //       this.id = param['cat_id'];

  //       console.log(this.activatedRoute.routeConfig.path);
  //       console.log(this);
        // if (this.id != undefined) {

  //        this._DatacallsService.getSubcat(this.id).subscribe(posts => {
  //  //console.log(posts.Data[0].data);
  //   this.posts=posts.Data;
  //     console.log(posts.Data);
  //        window.setTimeout(() => {
  //         var s = document.createElement("script");
  //         s.text = "TableDatatablesManaged.init();";
  //         this._elementRef.nativeElement.appendChild(s);
           
  //       }, 100);
  //  // console.log(this.posts);
  // });
      //  / else {
          this._DatacallsService.getCategory().subscribe(posts => {
            this.posts = posts.Data;
            console.log(posts.Data);
            window.setTimeout(() => {
              var s = document.createElement("script");
              s.text = "TableDatatablesManaged.init();";
              this._elementRef.nativeElement.appendChild(s);
            }, 100);
          });
      //   }
      // });
 
    // if(this.activatedRoute.routeConfig.path == 'category/subcat/:cat_id') {
    //     this.isSubcat = true;
    //     console.log("Subcat:" + this.isSubcat);
    // } else {
    //   this.isSubcat = false;
    //     console.log("Subcat:" + this.isSubcat);
    // }
  

  }


}
interface Post{
Message:string;
Status:number;
Success:string;
}