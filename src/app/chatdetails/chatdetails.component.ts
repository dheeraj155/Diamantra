import { Component, OnInit ,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';
import * as $ from 'jquery';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';

@Component({
  selector: 'app-chatdetails',
  templateUrl: './chatdetails.component.html',
  styleUrls: ['./chatdetails.component.css'],
  providers:[datacalls]
})
export class ChatdetailsComponent implements OnInit {
  posts;
  receiver_id;
  id;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls,private Router:Router,private _elementRef:ElementRef) { }

  ngOnInit() {


    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['id'];
        if (this.id != undefined) {
     this._datacalls.getSms(null,this.id,null).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();
    
    // this.dtTrigger.next();
      console.log('a:',posts.Data);
     
   // console.log(this.posts);
  });
    
}}
);
  

      }
    }
      
