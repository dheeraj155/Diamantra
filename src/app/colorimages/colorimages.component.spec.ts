import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorimagesComponent } from './colorimages.component';

describe('ColorimagesComponent', () => {
  let component: ColorimagesComponent;
  let fixture: ComponentFixture<ColorimagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorimagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
