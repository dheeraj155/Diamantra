import { Component, OnInit , NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as FileSaver from 'file-saver';
import {Router} from '@angular/router'
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute} from '@angular/router';
// import {NgxPaginationModule} from 'ngx-pagination';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';

@Component({
  selector: 'app-colorimages',
  templateUrl: './colorimages.component.html',
  styleUrls: ['./colorimages.component.css'],
  providers:[datacalls]
})
export class ColorimagesComponent implements OnInit {
  jsonPara = {};
  product_id;
  posts;
  data;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef, private fb:FormBuilder,private Router:Router,private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.product_id = param['product_id'];
        console.log(this)

this.jsonPara={"app_id":16,"cat_id":null,"product_id":this.product_id}
        this._DatacallsService.getProduct(this.jsonPara).subscribe(
          posts =>{
              this.posts=posts.Data
              console.log('inforamtion',this.posts);

          }
        );
  });

  this._DatacallsService.getColor(null,this.product_id).subscribe(
    posts =>{
        this.data=posts.Data
        console.log('inforamtion',this.posts);

    }
  );

}
}
