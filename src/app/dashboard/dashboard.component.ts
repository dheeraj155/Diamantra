import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[datacalls]
})
export class DashboardComponent implements OnInit {

	posts:Post[];
  a: any;
  order;
  category;
  subcategory;
  message;
  product;

  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }



  ngOnInit() {
     this._DatacallsService.getdasboard().subscribe(posts => {
    this.posts=posts.Data[0];
      console.log('a');
         window.setTimeout(() => {
          var s = document.createElement("script");
          s.text = "TableDatatablesManaged.init();";
          this._elementRef.nativeElement.appendChild(s);
        }, 100);
   console.log("Dashboard: ",this.posts['ordercount']);
   
   this.category=this.posts['totalcategory'];
   this.subcategory = this.posts['totalsubcategory'];
   this.product = this.posts['totalproduct'];
   this.order = this.posts['totalorder']; 
   
  });

  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}