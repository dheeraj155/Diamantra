import { Injectable,Inject } from '@angular/core';
import { Http,Headers,Response, RequestOptions , ResponseContentType } from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class datacalls {
 constructor(private http:Http) { }

 _url:'http://13.126.122.152:6001';

////Fruitz india
  getLogin(username,password){

    return this.http.get('http://localhost:3001/api/superlogin/'+username+'/'+password)
    .map(res=>res.json());


  }
   getPosts(){
    return this.http.get('http://35.154.141.107:3001/api/viewcategory')
    .map(res =>res.json());
    

  }
////Fruitz india
getCatdetails(id){

return this.http.get('http://13.126.122.152:6001/api/viewcat/'+id)
.map(res=>res.json());

}
////Fruitz india
getOrder(type){
return this.http.get('http://35.154.141.107:3001/api/vieworderdetailstype/23/'+type)
.map(res=>res.json());
}
////Fruitz india
getCategory(){

return this.http.get('http://13.126.122.152:6001/api/viewaryacategorydetails/23/0')
.map(res=>res.json());


}
////Fruitz india
 getAllCategory(){

return this.http.get('http://13.126.122.152:3001/api/viewallcategories/23')
.map(res=>res.json());


}
////Fruitz india
 getSubcat(id){

return this.http.get('http://13.126.122.152:6001/api/viewaryacategorydetails/23/'+id)
.map(res=>res.json());


}
getSubcatid(id,subcat_id){
  
  return this.http.get('http://13.126.122.152:6001/api/viewaryasubcategory/23/'+id+'/'+subcat_id)
  .map(res=>res.json());
  
  
  }
    getOffer(){
    return this.http.get('http://35.154.141.107:3001/api/viewaryaoffer')
    .map(res =>res.json());
    

  }
  ////Fruitz india
  getProproduct(){
    return this.http.get('http://35.154.141.107:3001/api/viewproproduct')
    .map(res =>res.json());
    

  }
  ////Fruitz india
   getProduct(json){
       

      console.log(json);
     var api = 'http://13.126.122.152:6001/api/viewdiamantraproduct';
    return this.http.post(api, json)
    .map(res =>res.json());
    

  }
  ////Fruitz india
   getProductdetails(id){
       

      console.log(id);
     var api = 'http://35.154.141.107:3001/api/aaryaproduct';
    return this.http.post(api, {"app_id":23,"cat_id":null,"customerid":0,"deviceid":null,"page":null,"pagesize":null,"price_id":null,"product_id":id,"search":"","status":1})
    .map(res =>res.json());
    

  }
 

getfruitorderrequest(order_id,product_id,page,pagesize){
    return this.http.get('http://13.126.122.152:6001/api/getfruitorderrequest/23/'+order_id+'/'+product_id+'/'+page+'/'+pagesize)
    .map(res=>res.json());
}

getdasboard(){
    return this.http.get('http://13.126.122.152:6001/api/dashboarddiamantracount/23')
    .map(res=>res.json());
}
////Fruitz india
postLogin(json){
    return this.http.post('http://35.154.141.107:3001/api/applogin', json)
    .map(res=>res.json());
  }
////Fruitz india
  getImageSliders(image_slider_id){
    return this.http.get('http://13.126.122.152:6001/api/viewimageslider/23/'+image_slider_id+'/1')
    .map(res=>res.json());
}

addbrochures(json){
  return this.http.post('http://13.126.122.152:6003/api/insertdicksonbrochures', json)
  .map(res=>res.json());
}

getBrochures(id){
  return this.http.get('http://13.126.122.152:6001/api/viewdicksonbrochures/23/'+id)
  .map(res=>res.json());
}
// this._url+

addCategory(json){
  return this.http.post('http://13.126.122.152:6002/api/aryacategory', json)
  .map(res=>res.json());
}
addImageSlider(json){
  return this.http.post('http://13.126.122.152:6003/api/imageslider', json)
  .map(res=>res.json());
}

addProduct(json){
  return this.http.post('http://13.126.122.152:6003/api/diamantraproduct', json)
  .map(res=>res.json());
}

addShowroom(json){
  return this.http.post('http://13.126.122.152:6003/api/insertdicksonshowroom', json)
  .map(res=>res.json());
}

getShowroom(id){
  return this.http.get('http://13.126.122.152:6001/api/viewdicksonshowroom/23/'+id)
  .map(res=>res.json());
}

getQuery(id,page,pagesize){
  return this.http.get('http://13.126.122.152:6001/api/viewdicksonquery/23/'+id+'/'+page+'/'+pagesize)
  .map(res=>res.json());
}

login(json){
  
return this.http.post('http://13.126.122.152:6001/api/memberlogin',json)
.map(res=>res.json());
}

getSms(id,receiver_id,answer_id){
  return this.http.get('http://13.126.122.152:6001/api/viewdicksonadminmsg/23/'+id+'/'+receiver_id+'/'+answer_id)
  .map(res=>res.json());
}
getuser(usertype,user_id){
  return this.http.get('http://13.126.122.152:6001/api/viewuser/23/1/'+usertype+'/'+user_id)
  .map(res=>res.json());
}
// add user 
addUser(json){
  return this.http.post('http://13.126.122.152:6001/api/insertuser', json)
  .map(res=>res.json());
}

// add user 
addcolorimages(json){
  return this.http.post('http://13.126.122.152:6003/api/insertdicksoncolorimage', json)
  .map(res=>res.json());
}
getColor(id,product_id){
  return this.http.get('http://13.126.122.152:6001/api/viewdicksoncolorimages/23/'+id+'/'+product_id)
  .map(res=>res.json());
}

getShape(){
  return this.http.get('http://myappcenter.co.in/testapi/api/viewdiamondshape/23/null')
  .map(res=>res.json());
}
addshape(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertdiamondshape', json)
  .map(res=>res.json());
}
getClarity(id){
  return this.http.get('http://myappcenter.co.in/testapi/api/viewdiamondclarity/23/'+id)
  .map(res=>res.json());
}
addClarity(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertdiamondclarity', json)
  .map(res=>res.json());
}
getColourstone(id){
  return this.http.get('http://myappcenter.co.in/testapi/api/viewcolourstone/23/'+id)
  .map(res=>res.json());
}
addColourstone(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertcolourstone', json)
  .map(res=>res.json());
}
getKarat(id){
  return this.http.get('http://myappcenter.co.in/testapi/api/viewgoldkarat/23/'+id)
  .map(res=>res.json());
}
addKarat(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertgoldkarat', json)
  .map(res=>res.json());
}
getDiamondsize(shape_id,id){
  return this.http.get('http://myappcenter.co.in/testapi/api/viewdiamondsize/23/'+shape_id+'/'+id)
  .map(res=>res.json());
}
addDiamondsize(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertdiamondsize', json)
  .map(res=>res.json());
}
getDiamondweight(shape_id,id){
  return this.http.get('http://13.126.122.152:6001/api/viewdiamantraweight/23/'+shape_id+'/'+id)
  .map(res=>res.json());
}
addDiamondweight(json){
  return this.http.post('http://13.126.122.152:6001/api/insertdiamantraweight', json)
  .map(res=>res.json());
}
getDiamondrate(clarity_id,shape_id,size_id,id){
  return this.http.get(' http://myappcenter.co.in/testapi/api/viewdiamantradiamondrate/23/'+clarity_id+'/'+shape_id+'/'+size_id+'/'+id)
  .map(res=>res.json());
}
addDiamondrate(json){
  return this.http.post('http://myappcenter.co.in/testapi/api/insertdiamantradiamondrate', json)
  .map(res=>res.json());
} 
getNewOrder(json){
  

 console.log(json);
var api = 'http://myappcenter.co.in/testapi/api/viewdiamantraorder';
return this.http.post(api, json)
.map(res =>res.json());


}
}
