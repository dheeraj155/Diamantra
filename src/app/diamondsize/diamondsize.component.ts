import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs/Rx';
import { datacalls } from '../datacalls.service';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-diamondsize',
  templateUrl: './diamondsize.component.html',
  styleUrls: ['./diamondsize.component.css'],
  providers:[datacalls]
})
export class DiamondsizeComponent implements OnInit {

 posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  shape_id;
  constructor(private _fb: FormBuilder,private activatedRoute:ActivatedRoute, private _http: Http, private _datacalls: datacalls, private changeDetectorRef: ChangeDetectorRef,private fb:FormBuilder,private Router:Router ) {
  }

  ngOnInit() {

     this.shape_id=this.activatedRoute.snapshot.params['shape_id']
          console.log(this.shape_id)
          if (this.shape_id != undefined) {
  
     this._datacalls.getDiamondsize(this.shape_id,null).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();

      console.log(this.posts=posts.Data);
  });
 
  }
  }
}
interface Post{
Message:string;
Status:number;
Success:string;
}