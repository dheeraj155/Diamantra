import { Component, OnInit, ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-karat',
  templateUrl: './karat.component.html',
  styleUrls: ['./karat.component.css'],
  providers:[datacalls]
})
export class KaratComponent implements OnInit {

  posts:Post[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  a: any;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  }

  ngOnInit() {
     this._DatacallsService.getKarat(null).subscribe(posts => {
    this.posts=posts.Data;
    this.dtTrigger.next();

      console.log(this.posts=posts.Data);
  });
 
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}