import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import { FormBuilder,FormGroup,Validators} from '@angular/forms';
  import {datacalls} from '../datacalls.service';
import { Router} from '@angular/router';
import * as $ from 'jquery';
import {User} from '../_models/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
    providers:[datacalls],
})
export class LoginComponent implements OnInit {



  @Output() getLoggedInName: EventEmitter<any> = new EventEmitter();
  alertClass;
  errorMsg;
  user:User;
   public loginForm:FormGroup;
  constructor(private fb:FormBuilder,public _datacalls:datacalls,private Router:Router){
  
  this.loginForm = this.fb.group({
     mobno: ['', Validators.required],
    password: ['', Validators.required],
    type:['', Validators.required]
  });
  }
  
    ngOnInit() {
   $('body').addClass('login');
  
      
    }
  
     logout() {
      sessionStorage.removeItem("currentuser");
      this.Router.navigate(['Login']);
    }
  
  doLogin(event){
    let formData={
      'app_id':23,
      'mobile_no':this.loginForm.value.mobno,
    'password':this.loginForm.value.password ,
    // 'user_type':'admin'
    };
  
    
  this._datacalls.login(formData).subscribe(
        data => {
  
          console.log('data:',data)
          
         if(data['Success']==true){
           
             this.user = { "mobile" :data.Data[0].mobile_no  ,"user" : data.Data[0].usertype};
           
          //  this.getLoggedInName.emit(true);
            
                sessionStorage.setItem('currentuser',JSON.stringify(this.user));
           //   this.Router.navigate([this.returnUrl]);
                console.log('current:',this.user.user)
              if(this.user.user=='admin'){
  
                this.Router.navigate(['dashboard'])
  
              }
               
              else {
            this.alertClass = "alert alert-danger text-center alert-dismissible";
            this.errorMsg = 'Something went wrong';
           
          }
            
          }
          
           
        }
   );
  
  }
 
}
