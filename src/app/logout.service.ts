import { Injectable } from '@angular/core';
import { Router} from '@angular/router';
import {User} from 'app/_models/user';
// import {datacalls} from './datacalls.service';

@Injectable()
export class LogoutService {
user;
  constructor(private Router:Router) { }
  
 logout(){

  // JSON.parse( localStorage.getItem('currentuser'));
       sessionStorage.removeItem('currentuser');
      this.Router.navigate(['login']);
    
  }

}
