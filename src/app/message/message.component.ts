import { Component, OnInit , NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  providers:[datacalls]
})
export class MessageComponent implements OnInit {
  posts;
  id;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    

    this._DatacallsService.getuser('customer',null).subscribe(posts => {
      //console.log(posts.Data[0].data);
       this.posts=posts.Data;
         console.log('a',posts.Data);
         this.dtTrigger.next();
         
      // console.log(this.posts);
     });
  }

  }


