import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
   providers:[datacalls],
})
export class OrderComponent implements OnInit {

  tableWidget: any = {};
 posts:Post[];
  a: any;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  jsonPara = {
    "app_id":23,
    "product_id":null,
    "customer_id":null,
    "gold_karat_id":null,
    "diamantra_diamond_rate_id":null,
    "colour_stone_id":null,
    "diamantra_order_id":null
    }
    ;
  constructor(private _DatacallsService:datacalls,private _elementRef:ElementRef) {
  
   // console.log('b');
  
    
   
  }
ngAfterViewInit(){
  this.initDatatable();
}

private initDatatable(): void {
    // debugger
    // let exampleId: any = $('#example');
    // this.tableWidget = exampleId.DataTable({
    //   select: true
    // });
 
  }

  ngOnInit() {
   // console.log('a');
  //  this.dtOptions = {
  //   pagingType: 'full_numbers'
  // };

     this._DatacallsService.getNewOrder(this.jsonPara).subscribe(posts => {
   //console.log(posts.Data[0].data);
    this.posts=posts.Data;
      console.log('a',posts.Data);
      this.dtTrigger.next();
   // console.log(this.posts);
  });
 
  }
}
interface Post{
Message:string;
Status:number;
Success:string;
}