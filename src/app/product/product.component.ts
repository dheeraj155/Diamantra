import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import * as FileSaver from 'file-saver';
import {ActivatedRoute,Router} from '@angular/router'
import * as $ from 'jquery';
import { Subject } from 'rxjs/Rx';
// import {NgxPaginationModule} from 'ngx-pagination';
import { FileUploader } from 'ng2-file-upload';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
// import { Subject } from 'rxjs/Rx';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
   providers:[datacalls],
})
export class ProductComponent implements OnInit {
public productForm: FormGroup;
   tableWidget: any = {};
 posts:Post[];
  a: any;
  id;
  subcat_id;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  jsonPara :any={}
  
  constructor(private _DatacallsService:datacalls,private activatedRoute:ActivatedRoute,private _elementRef:ElementRef, private fb:FormBuilder,private Router:Router) {
  
   // console.log('b');
      this.productForm = this.fb.group({

            Excelfile: ['', Validators.required]

        });
    
   
  }

ngAfterViewInit(){
  this.initDatatable();
}

private initDatatable(): void {
    // debugger
    // let exampleId: any = $('#example');
    // this.tableWidget = exampleId.DataTable({
    //   select: true
    // });
 
  }

  ngOnInit() {


    this.dtOptions = {
      pagingType: 'full_numbers'
    };
  //   let formData= new FormData() ;
  //  formData.append('app_id' , this.jsonPara.app_id);
  //  formData.append('cat_id' , this.jsonPara.cat_id);
  //  formData.append('customerid' , this.jsonPara.customerid);
  //  formData.append('deviceid' , this.jsonPara.deviceid);
  //  formData.append('page' , this.jsonPara.page);
  //  formData.append('pagesize' , this.jsonPara.pagesize);
  //  formData.append('price_id' , this.jsonPara.price_id);
  //  formData.append('product_id' , this.jsonPara.product_id);
  //  formData.append('search' , this.jsonPara.search);
  //  formData.append('status' , this.jsonPara.status);




  
  this.activatedRoute.params.subscribe(
    (param: any) => {

      this.id = param['id'];
      this.subcat_id = param['subcat_id'];
      

      console.log("new:",this.activatedRoute.routeConfig.path);
      console.log(this);
      if (this.id != undefined) {
        if (this.subcat_id != undefined) {

//        this._DatacallsService.getSubcat(this.id).subscribe(posts => {
//  //console.log(posts.Data[0].data);
//   this.posts=posts.Data;
//     console.log(posts.Data);
//        window.setTimeout(() => {
//         var s = document.createElement("script");
//         s.text = "TableDatatablesManaged.init();";
//         this._elementRef.nativeElement.appendChild(s);
         
//       }, 100);
//  // console.log(this.posts);
// });
    //  / else {
      this.jsonPara = {"app_id":23,"category_id":this.id,"sub_category_id":this.subcat_id}
            this._DatacallsService.getProduct(this.jsonPara).subscribe(posts => {
        console.log(posts.Data);
         this.posts=posts.Data;
         this.dtTrigger.next();
           console.log('a');
              
        // console.log(this.posts);
       });
      }}
    });

}

url="";
http="";


 file: File;
  filesToUpload: Array<File>;
 
onChange(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }


UploadExcel(){
 console.log(this.filesToUpload)
     var formData: any = new FormData();
   formData.append("app_id",1); 

 this.makeFileRequest("http://35.154.141.107:4000/api/uploadaryaexcel",formData, this.filesToUpload).then((result) => {
      console.log(result);
       if(result['Success']==true){
          alert('excel updated successfully');
  // window.setTimeout(() => {
  //           this.Router.navigate(['productlist']);
  //         }, 200);
   // this.Router.navigate(['productlist']);
     
        }
        else {
     
        }
    }, (error) => {
      console.error(error);
    });

}

makeFileRequest(url: string, formData: FormData, files: Array<File>) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }
    
      xhr.onreadystatechange = function () { 
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

}
interface Post{
Message:string;
Status:number;
Success:string;
}
