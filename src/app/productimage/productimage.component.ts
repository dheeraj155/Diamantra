import { Component, OnInit, NgModule,ElementRef } from '@angular/core';
import {datacalls} from '../datacalls.service';
import * as $ from 'jquery';
import { ActivatedRoute} from '@angular/router';
import { Subject } from 'rxjs/Rx';
// import {NgxPaginationModule} from 'ngx-pagination';
import 'rxjs/Rx';

@Component({
  selector: 'app-productimage',
  templateUrl: './productimage.component.html',
  styleUrls: ['./productimage.component.css'],
  providers:[datacalls]
})
export class ProductimageComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  id;
  posts;
  jsonPara:any;
  constructor(private activatedRoute:ActivatedRoute, private _datacalls: datacalls,private _elementRef:ElementRef) { }

  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    this.activatedRoute.params.subscribe(
      (param: any) => {

        this.id = param['product_id'];
        

        if (this.id != undefined ) {
          this.jsonPara = {"app_id":23,"cat_id":null,"product_id":this.id};
          
     this._datacalls.getProduct(this.jsonPara).subscribe(posts => {
       
    this.posts=posts.Data;
    this.dtTrigger.next();
    
    // this.dtTrigger.next();
      console.log('a:',posts.Data);

   // console.log(this.posts);
  });
    
}}
);
  }
  }


