import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import{BrochuresComponent} from './brochures/brochures.component';
import{AddbrochuresComponent} from './addbrochures/addbrochures.component';
 import {AddcategoryComponent} from './addcategory/addcategory.component';
 import {AddsubcategoryComponent} from './addsubcategory/addsubcategory.component';
 import {OrderComponent} from './order/order.component';
import {ConsumerorderComponent} from './consumerorder/consumerorder.component';
import {ProductComponent} from './product/product.component';
import{AddproductComponent} from './addproduct/addproduct.component';
import { ImagesliderComponent } from './imageslider/imageslider.component';
import { AddimagesliderComponent } from './addimageslider/addimageslider.component';
import{ShowroomComponent} from './showroom/showroom.component';
import{AddshowroomComponent} from './addshowroom/addshowroom.component';
import{QueryComponent} from './query/query.component';
import{MessageComponent} from './message/message.component';
import{AuthgaurdGuard} from './authgaurd.guard';
import{ChatdetailsComponent} from './chatdetails/chatdetails.component';
import{AnswerComponent} from './answer/answer.component';
import{AddarchitectComponent} from './addarchitect/addarchitect.component';
import{ArchitectComponent} from './architect/architect.component';
import{ProductimageComponent} from './productimage/productimage.component';
import{AddcolorimagesComponent} from './addcolorimages/addcolorimages.component';
import{ColorimagesComponent} from './colorimages/colorimages.component';
import{ShapeComponent} from './shape/shape.component';
import{AddshapeComponent} from './addshape/addshape.component';
import{ClarityComponent} from './clarity/clarity.component';
import{AddclarityComponent} from './addclarity/addclarity.component';
import{ColourstoneComponent} from './colourstone/colourstone.component';
import{AddcolourstoneComponent} from './addcolourstone/addcolourstone.component';
import{DiamondweightComponent} from './diamondweight/diamondweight.component';
import{AdddiamondweightComponent} from './adddiamondweight/adddiamondweight.component';
import{DiamondrateComponent} from './diamondrate/diamondrate.component';
import{AdddiamondrateComponent} from './adddiamondrate/adddiamondrate.component';
import{DiamondsizeComponent} from './diamondsize/diamondsize.component';
import{AdddiamondsizeComponent} from './adddiamondsize/adddiamondsize.component';
import{KaratComponent} from './karat/karat.component';
import{AddkaratComponent} from './addkarat/addkarat.component';


export const SECURE_ROUTES: Routes = [
    {path:'dashboard', component: DashboardComponent ,canActivate : [AuthgaurdGuard]},
    {path:'category',component:CategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editcategory/:id',component:AddcategoryComponent ,canActivate : [AuthgaurdGuard]},
    // {path:'add',component:AddcategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'order',component:OrderComponent,canActivate : [AuthgaurdGuard]},
    // {path:'consumerlist',component:ConsumerlistComponent},
    // {path:'retailerOrder',component:ConsumerorderComponent},
    // {path:'distributorOrder',component:DistributorOrderComponent},
    // {path:'retailerlist',component:RetailerlistComponent},
    // {path:'promoproduct',component:PromoproductComponent},
    {path:'productlist',component:ProductComponent,canActivate : [AuthgaurdGuard]},
    {path:'productimage/:product_id',component:ProductimageComponent,canActivate : [AuthgaurdGuard]},
    {path:'colorimage/:product_id',component:ColorimagesComponent,canActivate : [AuthgaurdGuard]},
    {path:'productlist/:id/:subcat_id',component:ProductComponent,canActivate : [AuthgaurdGuard]},
    // {path:'offer',component:OfferComponent},
    // {path:'addoffer',component:AddofferComponent},
    // {path:'addpromoproduct',component:AddpromotionalproductComponent},
    {path:'addproduct/:id/:subcat_id',component:AddproductComponent,canActivate : [AuthgaurdGuard]},
    {path:'editproduct/:id/:subcat_id/:product_id',component:AddproductComponent,canActivate : [AuthgaurdGuard]},
    // {path:'distributor',component:DistributorComponent},
    {path:'productlist/addproduct/:id/:subcat_id/:product_id',component:AddproductComponent,canActivate : [AuthgaurdGuard]},
    // {path:'promoproduct/addpromoproduct/:id',component:AddpromotionalproductComponent},
    // {path:'offer/addoffer/:id',component:AddofferComponent},
    {path:'category/subcat/:id',component:CategoryComponent,canActivate : [AuthgaurdGuard]},
    {path:'addcategory/:id', component: AddcategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'category/addsubcategory/:parent_id', component: AddcategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'category/addsubcategory/:parent_id/:id', component: AddcategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'addcategory',component:AddcategoryComponent,canActivate : [AuthgaurdGuard]},
    // {path:'bulkupload',component:BulkuploadComponent},
    // {path:'changeUserType/:userId',component:AddUserTypeComponent},
    // {path:'changeUserType/:userId/:type',component:AddUserTypeComponent},
    {path:'addimageslider',component:AddimagesliderComponent,canActivate : [AuthgaurdGuard]},
    {path:'imageslider',component:ImagesliderComponent,canActivate : [AuthgaurdGuard]},
    {path:'editimageslider/:image_slider_id',component:AddimagesliderComponent,canActivate : [AuthgaurdGuard]},
    {path:'addbrochures',component:AddbrochuresComponent,canActivate : [AuthgaurdGuard]},
     {path:'brochures',component:BrochuresComponent,canActivate : [AuthgaurdGuard]},
     {path:'editbrochures/:dickson_brochures_id',component:AddbrochuresComponent,canActivate : [AuthgaurdGuard]},
     {path:'showroom',component:ShowroomComponent,canActivate : [AuthgaurdGuard]},
     {path:'addshowroom',component:AddshowroomComponent,canActivate : [AuthgaurdGuard]},
     {path:'editshowroom/:dickson_showroom_id',component:AddshowroomComponent,canActivate : [AuthgaurdGuard]},
    {path:'query',component:QueryComponent,canActivate : [AuthgaurdGuard]},
    {path:'message',component:MessageComponent,canActivate : [AuthgaurdGuard]},
    {path:'chatdetails/:id',component:ChatdetailsComponent,canActivate : [AuthgaurdGuard]},
    {path:'answer/:user_id/:dickson_msgadmin_id',component:AnswerComponent,canActivate : [AuthgaurdGuard]},
    {path:'architect',component:ArchitectComponent,canActivate : [AuthgaurdGuard]},
    {path:'addarchitect',component:AddarchitectComponent,canActivate : [AuthgaurdGuard]},
    {path:'editarchitect/:id',component:AddarchitectComponent,canActivate : [AuthgaurdGuard]},
    {path:'addcolorimages/:product_id',component:AddcolorimagesComponent,canActivate : [AuthgaurdGuard]},
    // {path:'addproduct',component:ProductComponent,canActivate : [AuthgaurdGuard]},

{path:'subcategory/:id',component:SubcategoryComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editsubcategory/:id/:subcat_id',component:AddsubcategoryComponent ,canActivate : [AuthgaurdGuard]},
 {path:'addsubcategory/:id',component:AddsubcategoryComponent ,canActivate : [AuthgaurdGuard]},

    // {path: "businessopportunityimages/:training_id", component: BusinessopportunityimagesComponent},
    // {path: "addbusinessopportunityimages/:training_id", component:AddbusinessopportunityimagesComponent },
    // {path: "editbusinessopportunityimages/:training_id/:training_images_id", component: AddbusinessopportunityimagesComponent},
    
{path:'clarity',component:ClarityComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editclarity/:id',component:AddclarityComponent ,canActivate : [AuthgaurdGuard]},
 {path:'addclarity',component:AddclarityComponent ,canActivate : [AuthgaurdGuard]},

{path:'shape',component:ShapeComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editshape/:id',component:AddshapeComponent ,canActivate : [AuthgaurdGuard]},
 {path:'addshape',component:AddshapeComponent ,canActivate : [AuthgaurdGuard]},

 {path:'colourstone',component:ColourstoneComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editcolourstone/:id',component:AddcolourstoneComponent ,canActivate : [AuthgaurdGuard]},
 {path:'addcolourstone',component:AddcolourstoneComponent ,canActivate : [AuthgaurdGuard]},
    
    {path:'karat',component:KaratComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editkarat/:id',component:AddkaratComponent ,canActivate : [AuthgaurdGuard]},
 {path:'addkarat',component:AddkaratComponent ,canActivate : [AuthgaurdGuard]},

 {path:'diamondsize/:shape_id',component:DiamondsizeComponent ,canActivate : [AuthgaurdGuard]},
    {path:'editdiamondsize/:shape_id/:id',component:AdddiamondsizeComponent ,canActivate : [AuthgaurdGuard]},
 {path:'adddiamondsize/:shape_id',component:AdddiamondsizeComponent ,canActivate : [AuthgaurdGuard]},

 {path:'diamondrate',component:DiamondrateComponent ,canActivate : [AuthgaurdGuard]},
 {path:'editdiamondrate/:id',component:AdddiamondrateComponent ,canActivate : [AuthgaurdGuard]},
{path:'adddiamondrate',component:AdddiamondrateComponent ,canActivate : [AuthgaurdGuard]},
  
 {path:'diamondweight/:shape_id',component:DiamondweightComponent ,canActivate : [AuthgaurdGuard]},
 {path:'editdiamondweight/:shape_id/:id',component:AdddiamondweightComponent ,canActivate : [AuthgaurdGuard]},
{path:'adddiamondweight/:shape_id',component:AdddiamondweightComponent ,canActivate : [AuthgaurdGuard]},


];