import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {


sidebarMenu = [
  {
    name: "Dashboard",
    path: "/dashboard",
    icon: "fa fa-home",
    class: ""
  },
  {
    name: "Image Slider",
    path: "/imageslider",
    icon: "fa fa-image",
    class: ""
  },
  {
    name: "Category",
    path: "/category",
    icon: "fa fa-list-alt",
    class: ""
  },
  // {
  //   name: "Product",
  //   path: "/productlist",
  //   icon: "fa fa-navicon",
  //   class: ""
  // },
  {
    name: "Order",
    path: "/order",
    icon: "fa fa-shopping-cart",
    class: ""
  },
  {
    name: "Diamond Clarity",
    path: "/clarity",
    icon: "fa fa-building",
    class: ""
  },
  {
    name: "Diamond Shape",
    path: "/shape",
    icon: "fa fa-building",
    class: ""
  },
  {
    name: "Diamond Rate",
    path: "/diamondrate",
    icon: "fa fa-building",
    class: ""
  },
  {
    name: "Colour Stone",
    path: "/colourstone",
    icon: "fa fa-building",
    class: ""
  },
  {
    name: "Gold Karat",
    path: "/karat",
    icon: "fa fa-building",
    class: ""
  },
  
  // {
  //   name: "Brochures",
  //   path: "/brochures",
  //   icon: "fa fa-book",
  //   class: ""
  // },
  // {
  //   name: "Architect",
  //   path: "/architect",
  //   icon: "fa fa-image",
  //   class: ""
  // },
  // {
  //   name: "Query",
  //   path: "/query",
  //   icon: "fa fa-question",
  //   class: ""
  // },
  // {
  //   name: "Message",
  //   path: "/message",
  //   icon: "fa fa-comments",
  //   class: ""
  // },
  
  

]

  constructor(private router: Router, private activatedRoute:ActivatedRoute) {
      router.events.subscribe((val) => {
        
        for(var i=0; i<this.sidebarMenu.length; i++){
          if (this.sidebarMenu[i].path == this.router.routerState.snapshot.url) {
            this.sidebarMenu[i].class ="active";
          } else {
            this.sidebarMenu[i].class ="";
          } 
        }
    });
   }

  onClick(name){
    for(var i=0; i<this.sidebarMenu.length; i++){
      if (this.sidebarMenu[i].name == name) {
        this.sidebarMenu[i].class ="active";
      } else {
        this.sidebarMenu[i].class ="";
      } 
    }
  }

  ngOnInit() {
  }

}
